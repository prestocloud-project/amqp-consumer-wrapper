package consumer;

import com.rabbitmq.client.*;
import exceptions.AlreadySubscribedException;
import exceptions.BackupException;
import exceptions.NoHostException;
import exceptions.SubscribingFailedException;

import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.util.function.BiConsumer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

public class AmqpConsumer extends RabbitMqConsumer {

    private ConnectionFactory connectionFactory;
    private Connection connection = null;
    private Channel channel = null;
    private Address[] addresses;

    private boolean durable = true;
    private boolean autoDelete = false;

    private boolean exclusive = false;
    private int hoursQueueToLive = 24;
    private int prefetchCount = 1000;
    private String exchangeName = "presto.cloud"; //"amq.topic";
    private BuiltinExchangeType exchangeType = BuiltinExchangeType.TOPIC;

    private String tag;

    public AmqpConsumer(String address, boolean isDNS, String topic, BiConsumer<String, String> messageConsumer) {
        super(address, isDNS, topic, messageConsumer);
    }

    public AmqpConsumer(String address, boolean isDNS, BiConsumer<String, String> messageConsumer) {
        super(address, isDNS, null, messageConsumer);
    }

    @Override
    public boolean isDurable() {
        return durable;
    }

    @Override
    public void setDurable(boolean durable) {
        this.durable = durable;
    }

    @Override
    public boolean isAutoDelete() {
        return autoDelete;
    }

    @Override
    public void setAutoDelete(boolean autoDelete) {
        this.autoDelete = autoDelete;
    }

    public boolean isExclusive() {
        return exclusive;
    }

    public void setExclusive(boolean exclusive) {
        this.exclusive = exclusive;
    }

    public int getHoursQueueToLive() {
        return hoursQueueToLive;
    }

    public void setHoursQueueToLive(int hoursQueueToLive) {
        this.hoursQueueToLive = hoursQueueToLive;
    }

    public int getPrefetchCount() {
        return prefetchCount;
    }

    public void setPrefetchCount(int prefetchCount) {
        this.prefetchCount = prefetchCount;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public BuiltinExchangeType getExchangeType() {
        return exchangeType;
    }

    public void setNewTopicExchange(String exchangeName) {
        setNewExchange(exchangeName, BuiltinExchangeType.TOPIC);
    }

    public void setNewExchange(String exchangeName, BuiltinExchangeType exchangeType) {
        this.exchangeName = exchangeName;
        this.exchangeType = exchangeType;
    }

    private ExceptionHandler getNewExceptionHandler() {
        return new ExceptionHandler() {
            @Override
            public void handleUnexpectedConnectionDriverException(Connection connection, Throwable throwable) {
                logger.info("handleUnexpectedConnectionDriverException");
                loggException((Exception) throwable);
            }

            @Override
            public void handleReturnListenerException(Channel channel, Throwable throwable) {
                logger.info("handleReturnListenerException");
                loggException((Exception) throwable);
            }

            @Override
            public void handleConfirmListenerException(Channel channel, Throwable throwable) {
                logger.info("handleConfirmListenerException");
                loggException((Exception) throwable);
            }

            @Override
            public void handleBlockedListenerException(Connection connection, Throwable throwable) {
                logger.info("handleBlockedListenerException");
                loggException((Exception) throwable);
            }

            @Override
            public void handleConsumerException(Channel channel, Throwable throwable, Consumer consumer, String s, String s1) {
                logger.info("handleConsumerException");
                loggException((Exception) throwable);
            }

            @Override
            public void handleConnectionRecoveryException(Connection connection, Throwable throwable) {
                logger.info("handleConnectionRecoveryException");
                if (throwable instanceof SSLHandshakeException && throwable.getMessage().contains("certificate_expired")) {
                    connection.abort();
                    tearDownConnection();
                    amqpTrySSL();
                    try {
                        subscribe();
                    } catch (AlreadySubscribedException | SubscribingFailedException | NoHostException | IOException e) {
                        loggException(e);
                        tearDownConnection();
                    }
                } else {
                    loggException((Exception) throwable);
                }
            }

            @Override
            public void handleChannelRecoveryException(Channel channel, Throwable throwable) {
                logger.info("handleChannelRecoveryException");
            }

            @Override
            public void handleTopologyRecoveryException(Connection connection, Channel channel, TopologyRecoveryException e) {
                logger.info("handleTopologyRecoveryException");
            }
        };
    }

    @Override
    protected void setUpConnection() {
        connectionFactory = new ConnectionFactory();

        connectionFactory.setVirtualHost(vhost);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);

        connectionFactory.setAutomaticRecoveryEnabled(true);
        connectionFactory.setTopologyRecoveryEnabled(true);
        connectionFactory.setNetworkRecoveryInterval(10000);
        connectionFactory.setConnectionTimeout(0);
        connectionFactory.setExceptionHandler(getNewExceptionHandler());

        amqpTrySSL();
    }

    private void amqpTrySSL() {
        protocolPart = ":5672";
        if (isSsl()) {
            try {
                prepareSSL();
                protocolPart = ":5671";
                connectionFactory.setSocketFactory(sslSocketFactory);
            } catch (Exception e) {
                logger.warn("[x] Connection without SSL!");
                loggException(e);
            }
        }

        updateAllIps();
    }

    @Override
    protected void updateAllIps() {
        addresses = null;
        if (dns != null) {
            if (!ipList.isEmpty()) {
                String allIps = ipList.stream().map(i -> i + protocolPart).collect(Collectors.joining(","));
                addresses = Address.parseAddresses(allIps);
            }
        } else if (host != null) {
            addresses = new Address[]{Address.parseAddress(host + protocolPart)};
        }
    }

    @Override
    protected void tearDownConnection() {
        try {
            if (channel != null) {
                if (channel.isOpen())
                    channel.close();
                channel.abort();
            }
        } catch (IOException | TimeoutException | ShutdownSignalException e) {
            logger.error("[x] Couldn't close client!");
            loggException(e);
        } finally {
            channel = null;
        }

        try {
            if (connection != null) {
                if (connection.isOpen())
                    connection.close();
                connection.abort();
            }
        } catch (IOException | ShutdownSignalException e) {
            logger.error("[x] Couldn't close connection!");
            loggException(e);
        } finally {
            connection = null;
        }

        logger.info("[x] Unsubscribe finished!");
    }

    @Override
    public void subscribe(String newTopic) throws AlreadySubscribedException, SubscribingFailedException, NoHostException, IOException {
        if (newTopic == null) {
            String errorMessage = "[x] Topic name can't be null";
            logger.error(errorMessage);
            throw new IOException(errorMessage);
        }

        if (connection != null && channel != null) {
            String errorMessage = "[x] You are already subscribe to this broker on " + topic +
                    "\n Create new instance of consumer class for subscribing to last topic.";
            logger.error(errorMessage);
            throw new AlreadySubscribedException(errorMessage);
        }

        try {
            createConnection();
        } catch (TimeoutException | IOException e) {
            String errorMessage = "[x] Couldn't establish connection!";
            logger.error(errorMessage);
            if (e instanceof SSLHandshakeException && e.getMessage().contains("certificate_expired")) {
                amqpTrySSL();
                try {
                    createConnection();
                } catch (TimeoutException | IOException e1) {
                    logger.error(errorMessage);
                    loggException(e1);
                    throw new SubscribingFailedException(errorMessage);
                }
            } else {
                loggException(e);
                throw new SubscribingFailedException(errorMessage);
            }
        }

        connection.addShutdownListener(e -> logger.info("[x] Connection unexpectedly lost"));

        try {
            channel = connection.createChannel();
        } catch (IOException e) {
            String errorMessage = "[x] Couldn't create channel!";
            logger.error(errorMessage);
            loggException(e);
            tearDownConnection();
            throw new SubscribingFailedException(errorMessage);
        }

        topic = newTopic;
        loadBackedUpClientName();

        ((Recoverable) channel).addRecoveryListener(new RecoveryListener() {
            @Override
            public void handleRecovery(Recoverable recoverable) {
                logger.info("[x] Waiting for \"{}\" data on {}", topic, channel.getConnection().getAddress().getHostAddress() + ":" + channel.getConnection().getPort());
            }

            @Override
            public void handleRecoveryStarted(Recoverable recoverable) {
                logger.info("[x] Reconnecting...");
            }
        });

        channel.addShutdownListener(e -> logger.info("[x] Connection on channel unexpectedly lost"));

        try {
            Map<String, Object> args = new HashMap<>();
            args.put("x-expires", hoursQueueToLive * 60 * 60 * 1000); // 1day

            channel.basicQos(prefetchCount);

            channel.exchangeDeclare(exchangeName, exchangeType.getType(), true);

            doBackup = clientName == null || clientName.isEmpty();

            if (doBackup)
                clientName = "amqp-subscription-rabbit" + System.nanoTime();

            channel.queueDeclare(clientName, durable, exclusive, autoDelete, args);

            channel.queueBind(clientName, exchangeName, topic);

            try {
                backUpClientNameIfNeeded();
            } catch (BackupException e) {
                logger.warn(e.getMessage());
            }

            tag = channel.basicConsume(clientName, false, new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(
                        String consumerTag,
                        Envelope envelope,
                        AMQP.BasicProperties properties,
                        byte[] body
                ) throws IOException {
                    String message = new String(body, "UTF-8");
                    String topic = envelope.getRoutingKey();
                    messageConsumer.accept(message, topic);
                    logger.info("[x] Consume message: {}\n from topic: {}", message, topic);
                    channel.basicAck(envelope.getDeliveryTag(), false);
                }
            });
        } catch (IOException e) {
            String errorMessage = "[x] Couldn't establish connection!";
            logger.error(errorMessage);
            loggException(e);
            unsubscribe();
            throw new SubscribingFailedException(errorMessage);
        }

        logger.info("[x] Waiting for \"{}\" data on {}", topic, channel.getConnection().getAddress().getHostAddress() + ":" + channel.getConnection().getPort());
    }

    private void createConnection() throws IOException, TimeoutException, NoHostException {
        if (addresses != null)
            connection = connectionFactory.newConnection(addresses);
        else {
            String errorMessage = "[x] No host provided or DNS didn't return any record!";
            logger.error(errorMessage);
            throw new NoHostException(errorMessage);
        }
    }

    @Override
    public void unsubscribe() {
        try {
            if (!tag.isEmpty()) {
                channel.basicCancel(tag);
            }
        } catch (IOException e) {
            logger.error("[x] Couldn't cancel subscribing!");
            loggException(e);
        } finally {
            tag = "";
            tearDownConnection();
        }
    }

//    @Override
//    public void ackMessage(long messageId, int qoS) {
//        if (!autoAck) {
//            try {
//                channel.basicAck(messageId, false);
//            } catch (IOException e) {
//                loggException(e);
//            }
//        }
//    }
//
//    public void nackMessage(long messageId) {
//        if (!autoAck) {
//            try {
//                //rejects a single message, asking the broker to requeue it
//                channel.basicNack(messageId, false, true);
//            } catch (IOException e) {
//                loggException(e);
//            }
//        }
//    }

//    public static void main(String argv[]) {
//        String topic = "test.#";
//        java.util.function.BiConsumer<String, String> consumerFunction = AmqpConsumer::doSomething;
//        AmqpConsumer consumer = new AmqpConsumer("3.120.91.124", true, consumerFunction);
//        consumer.setUsernameAndPassword("nissatech", "9r3570cl0ud!");
//        consumer.useSSL("3.120.91.124", "s.pV8wvr4dkuq5ii476ddErT07");
//        boolean p = true;
//        while (p) {
//            try {
//                consumer.subscribe(topic);
//                p = false;
//            } catch (NoHostException | IOException | AlreadySubscribedException e1) {
//                e1.printStackTrace();
//                p = false;
//            } catch (SubscribingFailedException e) {
//                try {
//                    Thread.sleep(10000);
//                } catch (InterruptedException e1) {
//                    e1.printStackTrace();
//                }
//            }
//        }
//    }
//
//    private static void doSomething(String message, String topic) {
//    }
}
