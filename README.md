# AMQP MESSAGE CONSUMER WRAPPER

AMQP consumer wrapper is a library that uses [`com.rabbitmq.amqp-client` version `5.7.3`](https://mvnrepository.com/artifact/com.rabbitmq/amqp-client/5.7.3) and adds a layer for easier connectivity with the PrEsto Communication and Messaging Broker.

Libraries are available on Nissatech public Maven, and in order to use them you can:

- #### Add the repository and dependency tag to your pom.xml file:
```xml
<repository>
    <id>archiva.public</id>
    <name>Nissatech Public Repository</name>
    <url>https://maven.nissatech.com/repository/public/</url>
    <releases>
        <enabled>true</enabled>
    </releases>
    <snapshots>
        <enabled>false</enabled>
    </snapshots>
</repository>
```
```xml
<dependency>
    <groupId>com.nissatech.presto</groupId>
    <artifactId>amqp-consumer</artifactId>
    <version>7.0</version>
</dependency>
```
- #### Download jar and include it in your project:

https://maven.nissatech.com/repository/public/com/nissatech/presto/amqp-consumer/7.0/amqp-consumer-7.0-jar-with-dependencies.jar

## Basic usage
```java
import consumer.AmqpConsumer;
import exceptions.AlreadySubscribedException;
import exceptions.SubscribingFailedException;
import exceptions.NoHostException;
import java.io.IOException;

public class Receive_AMQP {
    public static void main(String argv[]) {
	String topic = "amqp.#";
	java.util.function.BiConsumer<String, String> consumerFunction = Receive_AMQP::doNothing;
	AmqpConsumer consumer = new AmqpConsumer("52.58.107.100", false, topic, consumerFunction);
	//consumer.setUsernameAndPassword("nissatech","**********");
	//consumer.useSSL("3.120.91.124", "**********");
	boolean p = true;
	while (p) {
            try {
                consumer.subscribe();
                p = false;
            } catch (NoHostException | IOException | AlreadySubscribedException e1) {
                e1.printStackTrace();
                p = false;
            } catch (SubscribingFailedException e) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    private static void doNothing(String message, String topic) {}
}
```
## Constructors

`AmqpConsumer(String host_or_dns_address, boolean is_DNS_address, String topic, BiConsumer<String, String> consumerFunction)` - creates instance of AmqpConsumer

| Param | Description | Required |
|---|:---:|---|
| host_or_dns_address | IP address of broker node or of the load balancing DNS | yes |
| is_DNS_address | is the first field DNS address or not | yes |
| topic | topic from which message should be received | no |
| consumerFunction | what to do with received message and full topic name | yes|
> Note: There are two constructors. One with all 4 arguments and another one with only required ones, so when subscribing you need to use method with newTopic argument.

> [Important note](https://www.rabbitmq.com/tutorials/tutorial-five-java.html): When a client subscribes to a topic, it can subscribe to the exact topic of a published message or 
it can use wildcards to subscribe to multiple topics simultaneously. ***A wildcard can only be used to subscribe to topics,
 not to publish a message***. There are two different kinds of wildcards: single-level (*) and multi-level (#).

By default:
- The [network recovery](https://www.rabbitmq.com/api-guide.html#recovery) interval is set to 10s, with this we enabled [automatic recovery](https://www.rabbitmq.com/api-guide.html#connection-recovery) so in
case of network failure consumer tries every 10s to reconnect, restore connection listeners, re-open
channels, and restore channel listeners. We also enabled [topology recovery](https://www.rabbitmq.com/api-guide.html#topology-recovery), which involves
recovery of exchanges, queues, bindings, and consumers. Also, connection timeout
(connection establishment timeout in milliseconds; zero for infinite) is set to 0.
- Port is automatically set to 5672. When the user enables SSL/TLS on the initiation of a connection, the port is changed to 5671.
- The exception handler is added to connection factory so when reconnect is started with an
expired certificate, the certificate is renewed and the process continues without errors.

## Setters

`void setUsernameAndPassword(String username, String password)` - set username and password for connecting to the broker

`void setVhost(String vhost)` - set [virtual host](https://www.rabbitmq.com/vhosts.html) for connecting to the broker

`void useSSL(String vault, String token)` - set vault address and token for SSL/TLS connection
- sslSockerFactory is prepared with the certificate
  received from the Vault server (TLS 1.2) and added to the connection factory. If it failed to create this
  factory it logs a warning and set connection without SSL.

`void turnOnSsl(boolean ssl)` - turn on/off usage of SSL/TLS certificates

`void setClientName(String clientName)` - set name of the queue

`void setDurable(boolean durable)` - set durability of the queue (if true the queue will survive a broker restart)

`void setAutoDelete(boolean autoDelete)` - set auto delete option of the queue (if true queue that has had at least one consumer is deleted when last consumer unsubscribes)

`void setExclusive(boolean exclusive)` - set queue exclusivity (used by only one connection and the queue will be deleted when that connection closes)

`void setHoursQueueToLive(int hoursQueueToLive)` - set queue time to live in hours (without active consumer)

`void setPrefetchCount(int prefetchCount)` - set consumer prefetch count

`void loadBackedUpClientName()` - load backuped queue name from file (used to restore state after consumer app goes down)

`void setNewExchange(String exchangeName, BuiltinExchangeType exchangeType)` - set new exchange name and type

`void setNewTopicExchange(String exchangeName)` - set new topic exchange name

## Getters

`String getUsername()` - get current username

`String getPassword()` - get current password
- for now, it uses the default (guest) username and password.

`String getVhost()` - get current virtual host
- default vhost is "/", and probably there is no need for some other, but we enable the user to
  set vhost if later we decide to create specific vhost for specific users.
  
`boolean isSsl()` - check if connection is secure

`String getClientName()` - get current queue name

`boolean isDurable()` - check if queue is durable

`boolean isAutoDelete()` - check if queue is auto-delete

`boolean isExclusive()` - check if queue is exclusive

`int getHoursQueueToLive()` - get queue time to live in hours

`int getPrefetchCount()` - get consumer prefetch count

`void backUpClientNameIfNeeded()` - backup client name to file (for later reconnection to same queue)  

`String getExchangeName()` - get set exchange name

`BuiltinExchangeType getExchangeType()` - get exchange type

## Subscribe methods

`void subscribe(String newTopic)` - subscribe to the broker

| Param | Description | Required | Default
|---|:---:|---|---|
| newTopic | topic name to which message should be sent to | no | topic set in constructor |

> Note: There are 4 versions of this method with a different number of arguments

`void unsubscribe()` - unsubscribing from topic, disconnecting the client and closing the connection

Subscribing checks if the topic is not null, checks if no other subscription already exists on this
instance, tries to create connection and channel, renews the certificate if needed, adds listeners,
declares exchange and queue, binds that queue to exchange, backups queue and topic name and
starts consuming:
- Check if the topic name that user try to connect isn’t null. Log error message and return
IOException.
- Because of a lot of parameters for consuming and because messages from other topics are
probably processed in a different way, the user needs to create one instance of the class for
every subscription to a different topic. So method checks if some connection already exists. Log
error message and return AlreadySubscribedException if this is the case.
- After this, it tries to create a connection and a channel. If the connection fails because of an
expired certificate, the certificate is renewed and new connection created. If it fails for some
other reason, it logs the error and returns SubscribingFailedException.
- Add ShutdownListener to connection and channel, so listener can log when the connection is lost.
Add RecoveryListener to channel so the user gets a log when recovery is started and
finished.
- Limit of unacknowledged messages on a channel (prefetchCount) is set to 1000. User can
change this as well.
- By declaring exchange we verify that exchange exists, or create it if needed. This method
creates an exchange if it does not already exist, and if the exchange exists, verifies that it is
of correct and expected type. Default exchange is presto.cloud TOPIC exchange, and
probably there is no need for some other, but we enable the user to setNewExchange with
other name and type of exchange.
- We also declare a queue. For the queue name, we use client name property. If the client
name is null or an empty string, we create it like "amqp-subscription-rabbit" + nanoTime. User
can also set some client name, and this functionality is used for recovery of consumer
application. By default, the queue is durable, non-auto-delete, non-exclusive, and has 24
hours to live. All these parameters can be changed before subscribing.
- By binding queue, we notify the exchange that it can send data to that queue on a defined topic.
- After that queue name is backed up in file with the topic name.
- If some exception occurred in these steps, connections will be closed, error logged and
SubscribingFailedException will be sent to the user.
- After this, we start consuming messages. Message payload and topic name are processed
with consume function. If an application needs to persist data, then it should ensure the
data is persisted prior to returning from this function, as after returning from this function,
the message is considered to have been delivered (auto acknowledge), and will not be
reproducible. Consumer logs the data.
- In the end, the user can unsubscribe from the broker. With unsubscribing channel and
connection are also closed.